# gin201-ospf-lab



## Getting started

This repo contains the repo for the GIN201 lab on OSPF at Telecom Paris. 

- R1-R6 are OSPF routers
- PC1-PC3 are regular servers
- R4-R6 are in a multi-access network

## Objectives

- Understand OSPF protocol
- Perform a crash test to check the convergence time
- Understand the DR/BDR election process
- Configure multi-area OSPF

As extra, you can also configure OSPF for IPv6 (You will need to manually add the ospf6d daemon and configuration file).

## Lab description

The up-to-date lab description can be downloaded [here](https://partage.imt.fr/index.php/s/ngoHPYpLG2935SC).
